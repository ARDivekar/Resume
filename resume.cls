%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Medium Length Professional CV - RESUME CLASS FILE
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% This class file defines the structure and design of the template.
%
% Original header:
% Copyright (C) 2010 by Trey Hunner
%
% Copying and distribution of this file, with or without modification,
% are permitted in any medium without royalty provided the copyright
% notice and this notice are preserved. This file is offered as-is,
% without any warranty.
%
% Created by Trey Hunner and modified by www.LaTeXTemplates.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\ProvidesClass{resume}[2010/07/10 v0.9 Resume class]

\LoadClass[10pt,letterpaper]{article} % Font size and paper type

% XeLaTeX packages.
\usepackage{xltxtra}
\usepackage{fontspec}
\defaultfontfeatures{Ligatures=TeX}

% \setmainfont % Open Sans
% [Path=./ARDivekarFonts/Open Sans/,
% ItalicFont=OpenSans-Italic,
% BoldFont=OpenSans-Bold,
% BoldItalicFont=OpenSans-BoldItalic]
% {OpenSans-Regular.ttf}

% \setmainfont % Garamond
% [Path=./ARDivekarFonts/Garamond/,
% ItalicFont=AGaramondPro-Italic,
% BoldFont=AGaramondPro-Bold,
% BoldItalicFont=AGaramondPro-BoldItalic]
% {AGaramondPro-Regular.otf}

\setmainfont % Helvetica
[Path=./ARDivekarFonts/Helvetica/,
ItalicFont=Helvetica-Oblique,
BoldFont=BMWHelvetica-Bold,
BoldItalicFont=Helvetica-BoldOblique]
{Helvetica-Normal.otf}


% \setmainfont % Georgia
% [Path=./ARDivekarFonts/Georgia/,
% ItalicFont=Georgia Italic,
% BoldFont=Georgia Bold,
% BoldItalicFont=Georgia Italic]
% {Georgia.ttf}

% \setmainfont % Droid Sans
% [Path=./ARDivekarFonts/Droid Sans/,
% BoldFont=DROID-SANS.BOLD]
% {DROID-SANS.REGULAR.TTF}

\usepackage{lipsum}
\usepackage[parfill]{parskip} % Remove paragraph indentation
\usepackage{array} % Required for boldface (\bf and \bfseries) tabular columns
\usepackage[super]{nth}
\usepackage{ifthen} % Required for ifthenelse statements
\usepackage{textcomp}
\pagestyle{empty} % Suppress page numbers
\usepackage[hidelinks]{hyperref} % Allows hyperlinks, without the ugly neon boxes. Source: http://tex.stackexchange.com/questions/823/remove-ugly-borders-around-clickable-cross-references-and-hyperlinks


%----------------------------------------------------------------------------------------
%	HEADINGS COMMANDS: Commands for printing name and address
%----------------------------------------------------------------------------------------

\def \name#1{\def\@name{#1}} % Defines the \name command to set name
\def \@name {} % Sets \@name to empty by default

\def \addressSep {$\diamond$} % Set default address separator to a diamond

% One, two or three address lines can be specified
\let \@addressone \relax
\let \@addresstwo \relax
\let \@addressthree \relax

% \address command can be used to set the first, second, and third address (last 2 optional)
\def \address #1{
  \@ifundefined{@addresstwo}{
    \def \@addresstwo {#1}
  }{
  \@ifundefined{@addressthree}{
  \def \@addressthree {#1}
  }{
     \def \@addressone {#1}
  }}
}

% \printaddress is used to style an address line (given as input)
\def \printaddress #1{
  \begingroup
    \def \\ {\addressSep\ }
    \centerline{#1}
  \endgroup
  \par
  \addressskip
}

% \printname is used to print the name as a page header
\def \printname {
  \begingroup
    % \hfil{\MakeUppercase{\namesize\bf \@name}}\hfil
    \hfil{\namesize\bf \@name}\hfil
    \nameskip\break
  \endgroup
}

%----------------------------------------------------------------------------------------
%	PRINT THE HEADING LINES
%----------------------------------------------------------------------------------------

\let\ori@document=\document
\renewcommand{\document}{
  \ori@document  % Begin document
  \printname % Print the name specified with \name
  \@ifundefined{@addressone}{}{ % Print the first address if specified
    \printaddress{\@addressone}}
  \@ifundefined{@addresstwo}{}{ % Print the second address if specified
    \printaddress{\@addresstwo}}
     \@ifundefined{@addressthree}{}{ % Print the third address if specified
    \printaddress{\@addressthree}}
}

%----------------------------------------------------------------------------------------
%	SECTION FORMATTING
%----------------------------------------------------------------------------------------


% Defines the rSection environment for the large sections within the CV
\newenvironment{rSection}[2]{ % 1st input argument - section name, 2nd input argument - relevant links.
  \sectionskip %
  \hbox to \textwidth{\MakeUppercase{\bf #1} \hfill #2} %
  \sectionlineskip %
  \hrule  % Section title and relevant links % Horizontal line
  \vspace{0em} % Space before first item in list
  \begin{list}{}{ % List for each individual item in the section
    \setlength{\leftmargin}{\sectionleftmarginval} % Margin within the section
  } %
  \item[] %
}{ %
  \end{list} %
} %


% Defines the rSection environment for the large sections within the CV
\newenvironment{rSectionWithOnlyList}[2]{ % 1st input argument - section name, 2nd input argument - relevant links.
  \sectionskip %
  \hbox to \textwidth{\MakeUppercase{\bf #1} \hfill #2} %
  \sectionlineskip %
  \hrule  % Section title and relevant links % Horizontal line
  \vspace{-0.65em} % Space before first item in list
  \begin{list}{}{ % List for each individual item in the section
    \setlength{\leftmargin}{\sectionleftmarginval} % Margin within the section
  } %
  \item[] %
}{ %
  \end{list} %
} %


%----------------------------------------------------------------------------------------
%	WORK EXPERIENCE FORMATTING
%----------------------------------------------------------------------------------------

\newenvironment{rSubsection}[4]{ % 4 input arguments - company name, year(s) employed, job title and location
	\vspace{0.0em}			% Space before each subsection
  {\bf #1 					% Bold company name
 	  \vspace{-0.1em}			% Space after company name/title of section (the thing in bold).
  }
  \hfill{#2 \hspace{-0.75em}} 				% Date on the right
  \ifthenelse{ \equal{#3}{} } {} { % If the third argument is not specified i.e. if it is {}, don't print the job title and location line.
    {  
      \vspace{0.15em}		% Space before the title and location (if that line exists)
  	  \\
    \em #3
    } \hfill {\em #4} 	% Italic job title and location
	  \vspace{-0.1em}		% Space after the title and location (if that line exists)
  }
  \vspace{-0.15em}			% Space before list of points.
  % Ref for \begin{list}: https://tex.stackexchange.com/a/375615
  \begin{list}{
    \sectionleftmargin \-\hspace{0.5em} $\textbullet$ \hspace{-0.5em}
  }
  {
    \leftmargin=0em % no indentation
  } 
  \itemsep -0.35em 			% Space between each item in the list (between, nor before or after).
  \vspace{-0.25em} % Compress items in list together for aesthetics
}
{
  \end{list}
  \vspace{0.35em} % Some space after the list of bullet points
}


\newenvironment{rSubsectionWithoutList}[4]{ % 4 input arguments - company name, year(s) employed, job title and location
	\vspace{-1.0em}			% Space before each subsection
  {\bf #1 					% Bold company name
 	  \vspace{-0.1em}			% Space after company name/title of section (the thing in bold).
  }
  \hfill{#2 \hspace{-0.75em}} 				% Date on the right
  \ifthenelse{ \equal{#3}{} } {} { % If the third argument is not specified i.e. if it is {}, don't print the job title and location line.
    {  
      \vspace{0.15em}		% Space before the title and location (if that line exists)
  	  \\
    \em #3
    } \hfill {\em #4} 	% Italic job title and location
	  \vspace{-0.1em}		% Space after the title and location (if that line exists)
  }
  \vspace{-0.15em}
}
{}

% The below commands define the whitespace after certain things in the document - they can be \smallskip, \medskip or \bigskip
\def\namesize{\Large} % Size of the name at the top of the document
\def\addressskip{\smallskip} % The space between the two address (or phone/email) lines
\def\sectionlineskip{\medskip} % The space above the horizontal line for each section
\def\nameskip{\medskip} % The space after your name at the top
\def\sectionskip{\medskip\vspace{0.65em}} % The space before the heading of each section
\def\smallindent{\-\hspace{1em}}
\def\sectionleftmargin{\-\hspace{1.5em}}
\def\sectionleftmarginval{0em}
